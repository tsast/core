/**
 * BackEnd API configuration
 * @memberOf config
 * @name api
 * @type {Object}
 * @property {String} server - Server address
 * @property {String} key - oAuth key
 * @property {String} token - oAuth token
 * @property {Object} old - Old system
 * @property {String} old.server - Server address
 * @property {String} old.user - Auth Username
 * @property {String} old.password - Auth Password
 */
module.exports = {
    server: 'http://backend',
    key: 'test',
    token: 'test'
};
