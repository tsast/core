# ЦАСТ ядро


## npm команды
```bash
// генерация документации
npm run doc

// публикация документации
npm run publishDoc

// Запуск тестов
npm run test

// Текущая версия миграции
npm run mcv

// Создание миграции
npm run mc <name>

// Проведение миграций
npm run mm

// Откат миграций на 1
npm run mr
```
