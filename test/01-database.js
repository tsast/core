'use strict';

const db = require('../knexfile');
db.pool = {
    min: 1,
    max: 1
};
const config = require('../config/local');
const knex = require('knex')(db);
const mssql = require('mssql');

describe('Database', () => {
    // TODO fix this on knex fixed connction messages
    describe('PostgreSQL', () => {
        console.log = function () {};
        describe('#connection', () => {
            it('database connected', () => {
                knex.raw('select 1+1 as result');
            });
        });

        describe('#migrations', () => {
            it('database migrate', () => {
                knex.migrate.latest(db);
            });
            it('database rollback', () => {
                knex.migrate.rollback(db);
            });
        });
    });
    describe('MS SQL', () => {
        describe('#connection', () => {
            it('database connected', () => {
                mssql.connect(config.db.ms);
            });
        });
    });
});
