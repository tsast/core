const path = require('path');
/**
 * Application config
 * @type {Object}
 * @namespace
 */
config = {};

/**
 * Init application core
 * @param {String} configPath - config directory path
 * @module core
 */
module.exports = function (configPath) {
    /**
     * lodash
     * @see {@link https://lodash.com/docs|Docs}
     * @type {lodash}
     * @global
     */
    _ = require('lodash');

    /**
     * Bluebird
     * @see {@link http://bluebirdjs.com/docs/api-reference.html|Docs}
     * @type {bluebird}
     * @global
     */
    Promise = require('bluebird');

    /**
     * Crontab
     * @see {@link https://www.npmjs.com/package/node-crontab|Docs}
     * @type {crontab}
     * @example
     *  *    *    *    *    *    *
     * ┬    ┬    ┬    ┬    ┬    ┬
     * │    │    │    │    │    |
     * │    │    │    │    │    └ day of week (0 - 7) (0 or 7 is Sun)
     * │    │    │    │    └───── month (1 - 12)
     * │    │    │    └────────── day of month (1 - 31)
     * │    │    └─────────────── hour (0 - 23)
     * │    └──────────────────── minute (0 - 59)
     * └───────────────────────── second (0 - 59, optional)
     * @example
     * crontab.scheduleJob('* * * * * *', function()  {
     *  console.log('every minute');
     * });
     * @global
     */
    crontab = require('node-crontab');

    /**
     * Application services
     * @see {@link http://docs.tsast.ru/services/|Docs}
     * @type {services}
     * @global
     */
    services = require('services');

    config = require('./config')(configPath);

    /**
     * Beanstalkd
     * @see {@link https://www.npmjs.com/package/beanstalkd|Docs}
     * if config.beanstalkd set host
     * @type {beanstalkd}
     * @global
     */
    beanstalkd = false;
    if (config.beanstalkd.hasOwnProperty('host') && config.beanstalkd.host.length > 0) {
        beanstalkd = require('beanstalkd');
    }

    /**
     * Moment.js
     * @see {@link http://momentjs.com/docs/|Docs}
     * @type {moment}
     * @global
     */
    moment = require('moment');
    moment.locale('ru');

    /**
     * PostgreSQL orm by knex
     * @see {@link http://knexjs.org|Docs}
     * @type {knex}
     * @global
     */
    pg = false;
    if (config.db.pg.host !== '') {
        pg = require('knex')({
            client: 'postgresql',
            connection: {
                host: config.db.pg.host,
                port: config.db.pg.port,
                user: config.db.pg.user,
                password: config.db.pg.password,
                database: config.db.pg.database
            },
            pool: {
                min: config.db.pg.pool.min,
                max: config.db.pg.pool.max
            }
        });
    }

    /**
     * MS SQL library
     * @see {@link https://www.npmjs.com/package/mssql|Docs}
     * @type {mssql}
     * @global
     */
    mssql = false;
    if (config.db.ms.host !== '') {
        mssql = require('mssql');
    }

    // Check and init crontab from config
    if (config.crontab && _.size(config.crontab) > 0) {
        _.forEach(config.crontab, (func, time) => {
            crontab.scheduleJob(time, func);
        });
    }

    /**
     * Redis client
     * @see {@link https://www.npmjs.com/package/redis|Docs}
     * if config.redis set host
     * @type {redis}
     * @global
     */
    redis = false;
    if (config.redis.hasOwnProperty('host') && config.redis.host.length > 0) {
        const redisClient = require('redis');
        redis = redisClient.createClient(config.redis);
    }

    /**
     * Express.js server
     * @see {@link http://expressjs.com/en/4x/api.html|Docs}
     * @type {express}
     * @global
     */
    server = require('express')();
    app = require('http').Server(server);
    /**
     * Socket.io
     * @see {@link http://socket.io/docs/|Docs}
     * @type {socket}
     * @global
     */
    io = require('socket.io')(app);
};

/**
 * Reuire and run class modules from array
 * @param {Array} modules - Modules array
 * @param {String} dir - Load modules from Directory
 */
module.exports.requireModules = function (modules, dir) {
    _.forEach(modules, (config, module) => {
        const m = require(path.join(dir, module));
        const c = new m(config);
    });
};
