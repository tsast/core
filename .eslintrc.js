module.exports = {
    "extends": "airbnb",
    "env": {
        "browser": true,
        "node": true,
        "mocha": true
    },
    "ecmaFeatures": {
        "experimentalObjectRestSpread": true
    },
    "globals": {
        "config": true,
        "_": true,
        "Promise": true,
        "crontab": true,
        "services": true,
        "beanstalkd": true,
        "moment": true,
        "pg": true,
        "mssql": true,
        "server": true,
        "io": true,
        "app": true,
        "redis": true
    },
    "rules": {
        "strict": 0,
        "no-unused-vars": 0,
        "no-param-reassign": 0,
        "func-names": 0,
        "new-cap": 0,
        "import/default": 0,
        "import/no-duplicates": 0,
        "import/named": 0,
        "import/namespace": 0,
        "import/no-unresolved": 0,
        "comma-dangle": 0,  // not sure why airbnb turned this on. gross!
        "indent": [2, 4, {"SwitchCase": 1}],
        "no-console": 0,
        "no-alert": 0
    }
};
