/**
 * Redis settings
 * @memberOf config
 * @name redis
 * @type {Object}
 * @property {String} host - Server address
 * @property {Number} port - Server port number
 * @property {Number} ttl - TTL data on seconds
 * @property {Number} db - Database index
 * @property {String} password - Server password
 * @property {String} prefix - Data prefix
 */
module.exports = {
    host: 'localhost',
    port: 6379,
    ttl: 60 * 24 * 365,
    db: 0,
    prefix: 'session:'
};
