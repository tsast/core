/**
 * API server configuration
 * @memberOf config
 * @name server
 * @type {Object}
 * @property {String} port - Server port
 */
module.exports = {
    port: 3000
};
