'use strict';
const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const services = require('services');

module.exports = function (configPath) {
    configPath = configPath || __dirname;
    // Include config files to global variable config
    let config = services.includeFilesFromDir(__dirname, ['index.js', 'local.js']);
    config = _.merge(config, services.includeFilesFromDir(configPath, ['index.js', 'local.js']));

    // Inject local.js if file exist
    if (fs.existsSync(path.join(configPath, '/local.js'))) {
        config = _.merge(config, require(path.join(configPath, 'local')));
    }
    return config;
};
