/**
 * Beanstalkd connection config
 *
 * [Official documentation]{@link https://www.npmjs.com/package/beanstalkd}
 *
 * @memberOf config
 * @name beanstalkd
 * @type {Object}
 * @property {String} host - Server host
 * @property {Number} port - Server port
 */
module.exports = {
    host: '',
    port: 11300
};
