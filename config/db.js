/**
 *
 * @memberOf config
 * @name db
 * @type {Object}
 * @property {Object} pg - PostgreSQL settings
 * @property {String} pg.host - Server host
 * @property {Number} pg.port - Server port number
 * @property {String} pg.user - Database user
 * @property {String} pg.password - User password
 * @property {String} pg.database - Database name
 * @property {Object} pg.pool - Database pools
 * @property {Number} pg.pool.min - min
 * @property {Number} pg.pool.max - max
 * @property {Object} ms - MS SQL settings
 * @property {String} ms.server - Server host
 * @property {String} ms.user - Database user
 * @property {String} ms.password - User password
 * @property {String} ms.database - Database name
 */
module.exports = {
    pg: {
        host: '',
        port: 5432,
        user: 'root',
        password: '',
        database: '',
        pool: {
            min: 2,
            max: 10
        }
    },
    ms: {
        server: '',
        user: 'root',
        password: '',
        database: ''
    }
};
