/**
 * Email configuration
 * @memberOf config
 * @name email
 * @type {Object}
 * @property {String} host - smtp host
 * @property {Number} port - smtp port
 * @property {String} user - smtp user
 * @property {Object} password - smtp password
 * @property {Boolean} ssl - ssl connection
 * @property {String} from - Email from
 */
module.exports = {
    host: 'localhost',
    port: 25,
    user: '',
    password: '',
    ssl: false,
    from: 'service@localhost'
};
