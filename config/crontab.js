/**
 * Global crontab configuration
 * @example
 *   * * * * * *
 * - - - - -
 * | | | | |
 * | | | | ----- День недели (0 - 7) (Воскресенье =0 или =7)
 * | | | ------- Месяц (1 - 12)
 * | | --------- День (1 - 31)
 * | ----------- Час (0 - 23)
 * ------------- Минута (0 - 59)
 * @example
 * '* * * * * *': function()  {
 *  console.log('every minute');
 * }
 * @memberOf config
 * @name crontab
 * @type {Object}
 */
module.exports = {};
