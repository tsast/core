const config = require('./config')();

module.exports = {
    client: 'postgresql',
    connection: {
        host: config.db.pg.host,
        port: config.db.pg.port,
        user: config.db.pg.user,
        password: config.db.pg.password,
        database: config.db.pg.database
    },
    pool: {
        min: config.db.pg.pool.min,
        max: config.db.pg.pool.max
    },
    migrations: {
        tableName: 'migrations',
        directory: './node_modules/migrations/migrations'
    }
};
